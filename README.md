# my-learning-resources

## Completed
* **Neural networks** from neuralnetworksanddeeplearning.com
* **Game Theory** by Ben Polak,Yale Youtube video and open yale course contents
* **Kernel PCA** http://www.mitpressjournals.org/doi/10.1162/089976698300017467 by Schölkopf and Smola
* **Deep learning** by Andrew Ng on coursera under name deeplearning.ai
* **Reinforcement learning** from https://youtu.be/2pWv7GOvuf0 RL course by David Silva
* **Machine Learning** by Andrew Ng  Youtube video for more detailed and slightly challenging content. Coursera for basic understanding and Programming Assignments
* **Big data and Hadoop** a basic introduction to big data by Udemy

## Going on
* **Recurrent Network** training difficulty by Yoshua Bengio,Razvan Pascanu, Tomas Mikolov https://arxiv.org/pdf/1211.5063
* **LSTM** by Hochreiter and Schmidhuber 



